import Vue from 'vue';
import VueRouter from 'vue-router';

import Home from '@/pages/home/Index.vue';
import News from '@/pages/news/Index.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
  },
  {
    path: '/news/:id',
    name: 'news',
    component: News,
  },
];

const router = new VueRouter({
  routes,
  mode: 'history',
});

export default router;
